#ifndef THEATERUTILITY_H
#define THEATERUTILITY_H
#include <string>
#include <vector>



class TheaterUtility
{

public:
    TheaterUtility();
    // This method is used for taking the input from the user.
    int getInputForInt(int choice, std::string message); //To take the input as an integer.
    std::string getInputForString(std::string choice,  std::string message); //Takes the input as string.
    std::string getInputForEventId(std::string eventId,std::string message); //To take the event id for the particular event.
    bool isValidEventId(std::string eventId); //validating the event id to check whether it is existing or not.
    int getInputForSeats(std::string eventId,int seats,std::string message); //Taking th einput for seats.
    bool isValidEventId(std::string eventId, int seats); //To check availability of seats to the given id.
    //  bool isSeatsValidForEventId(std::string eventId,int seats);
    std::vector <std::string> getStringToken(std::string record); //To split the record based on the delimeter.
    std::vector <std::string> getEventById(std::string eventId); //To get the event record by using event id.
    int getRand();// to generate random number for event id.
    bool isValidBookingId(std::string bookingId);//Validation for the booking id.
    int getInputForChoice(int choice,int maxChoice, std::string message);//To validate the input choice.
    std::string getStrings();
    std::string datepicker(std::string message);//To construct the dates.
        std::string getTodayDate();//To get today's date.
};

#endif // THEATERUTILITY_H
