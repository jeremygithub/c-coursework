#include "TheaterUtility.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <string>
#include <iostream>
#include "Customer.h"
#include <ctime>
TheaterUtility::TheaterUtility()
{

}
int TheaterUtility::getInputForChoice(int choice,int maxChoice, std::string message)
{
    std::cout<< message;
    std::cin >>  choice;
    while(!std::cin.good() ||  (choice>=maxChoice))
    {
        std::cout<<"ERROR: Faulty Input! try again .."<<"\n";
        std::cin.clear();
        std::cin.ignore(9999, '\n');
        std::cout<< message ;
        std::cin>> choice;
    }
   std:: cin.clear();
    std::cin.ignore(9999, '\n');
    return choice;
}
int TheaterUtility::getInputForInt(int choice, std::string message)
{
    std::cout<< message;
    std::cin>>  choice;
    while(!std::cin.good())
    {
        std::cout<<"ERROR: Faulty Input! try again .."<<"\n";
        std::cin.clear();
        std::cin.ignore(9999, '\n');
        std::cout<< message ;
        std::cin>> choice;
    }
    std::cin.clear();
    std::cin.ignore(9999, '\n');
    return choice;
}
std::string TheaterUtility::getInputForString(std::string input, std::string message)
{

    std::cout<< message;
    std::cin>>  input;
    while(!std::cin.good())
    {
        std::cout<<"ERROR: Faulty Input! try again .."<<"\n";
        std::cin.clear();
        std::cin.ignore(9999, '\n');
        std::cout<< "Enter input ";
        std::cin>> input;
    }
    std::cin.clear();
    std::cin.ignore(9999, '\n');
    return input;
}
std::string TheaterUtility::getInputForEventId(std::string eventId,std::string message)
{

    std::cout<< message;
    std::cin>>  eventId;
    while(!isValidEventId(eventId))
    {
        std::cout<<"ERROR: invalid eventId! try again .."<<"\n";
        std::cin.clear();
       std::cin.ignore(9999, '\n');
        std::cout<<"Enter Event Id: ";
        std::cin>> eventId;
    }
    std::cin.clear();
    std::cin.ignore(9999, '\n');
    return eventId;
}
bool TheaterUtility::isValidEventId(std::string eventId)
{
    TheaterUtility tu;
    std::string myText;
    std::ifstream MyReadFile("events.txt");
    while (getline (MyReadFile, myText))
    {
        std::vector<std::string> token=tu.getStringToken(myText);
        if (eventId.compare(token[0])==0)
        {

            MyReadFile.close();
            return true;
        }
    }
    MyReadFile.close();
    return false;
}


std::vector<std::string> TheaterUtility::getStringToken(std::string myText)
{
    std::vector <std::string> tokens;
    std::stringstream check1(myText);

    std::string intermediate;
    while(getline(check1, intermediate, '|'))
    {
        tokens.push_back(intermediate);
    }

    return tokens;
}
std::vector<std::string> TheaterUtility::getEventById(std::string eventId)
{

    TheaterUtility tu;
    std::string myText;
    std::ifstream MyReadFile("events.txt");
    std::vector<std::string> token;
    while (getline (MyReadFile, myText))
    {
        token=tu.getStringToken(myText);
        if (eventId.compare(token[0])==0)
        {
            MyReadFile.close();
            return token;
        }
    }
    MyReadFile.close();
    return token;

}


int TheaterUtility:: getInputForSeats(std::string eventId,int seats,std::string message)
{
    seats=getInputForInt(seats,"Enter No.of seats:");
    std::vector<std::string> token=getEventById(eventId);
    while(std::stoi(token[5])<seats)
    {
        std::cout<<"Entered seats are more than available seats \n";
        seats=getInputForInt(seats,"Enter No.of seats: ");
    }
    return seats;
}

int TheaterUtility:: getRand()
{
    std::srand(static_cast<unsigned int>(std::time(nullptr)));
    return std::rand() ;
}
std::string TheaterUtility::getStrings(){
    return "course";
}
std::string TheaterUtility::datepicker(std::string message)
{
    std::cout<< message<<std::endl;
    int day,month,year;
    bool flag;
   // std::cout <<((day>0 && day<31) || (month>0 && month<13) || (year>2001 && year<2023));
    do
    {
     day=getInputForInt(day,"Enter the Day: ");
        month =getInputForInt(month,"Enter the Month: ");
        year =getInputForInt(year,"Enter the Year: ");
        if(day>0 && day<31 && month>0 && month<13 && year>2001 && year<2023)
        {

            flag=false;
        }
        else{
            flag=true;
            std::cout<<"Error: Entered Date is not correct Please try again"<<std::endl;
        }
    }while(flag);

    return std::to_string(day)+"/"+std::to_string(month)+"/"+std::to_string(year);
}
std::string TheaterUtility::getTodayDate()
{
    time_t now = time(0);
    tm *ltm = localtime(&now);
    std::string currentDate=std::to_string(ltm->tm_mday) + "/" + std::to_string(1 + ltm->tm_mon)+"/"+std::to_string(1900 + ltm->tm_year);
    return currentDate;
}
