#ifndef CUSTOMER_H
#define CUSTOMER_H
#include <string>
#include <iostream>
#include <fstream>


class Customer
{
    public:
        Customer();

        std::string GetbookingId() { return bookingId; }
        void SetbookingId(std::string val) { bookingId = val; }
        std::string GeteventID() { return eventID; }
        void SeteventID(std::string val) { eventID = val; }
        std::string GeteventName() { return eventName; }
        void SeteventName(std::string val) { eventName = val; }
        std::string GeteventType() { return eventType; }
        void SeteventType(std::string val) { eventType = val; }
        std::string GetdateOnBooked() { return dateOnBooked; }
        void SetdateOnBooked(std::string val) { dateOnBooked = val; }
        std::string GetnoOfTicketsBooked() { return noOfTicketsBooked; }
        void SetnoOfTicketsBooked(std::string val) { noOfTicketsBooked = val; }
        std::string Getstatus() { return status; }
        void Setstatus(std::string val) { status = val; }

        void bookAnEvent();
        void menu();
        void mybookings();


    protected:

    private:
        std::string bookingId;
        std::string eventID;
        std::string eventName;
        std::string eventType;
        std::string dateOnBooked;
        std::string noOfTicketsBooked;
        std::string status;
};

#endif // CUSTOMER_H
