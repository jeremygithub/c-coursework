#ifndef STANDUPCOMEDY_H
#define STANDUPCOMEDY_H
#include "StandupComedy.h"
#include "Event.h"
#include<string>
class StandupComedy : public Event
{
    public:
        StandupComedy();


        std::string Getseats() { return seats; }
        void Setseats(std::string val) { seats = val; }



    private:
        std::string seats;
};

#endif // STANDUPCOMEDY_H
