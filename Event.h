#ifndef EVENT_H
#define EVENT_H

#include <string>


class Event
{
private:
    int eventId;
     std::string eventName;
    std::string eventStartDate;
   std::string eventEndDate;
    std::string numberOfSeats;
    std::string typeOfEvent;
public:
    Event();
    int geteventId()
    {
        return eventId;
    }
    void seteventId(int val)
    {
        eventId = val;
    }
    std::string geteventName()
    {
        return eventName;
    }
    void seteventName(std::string val)
    {
        eventName = val;
    }
   std::string geteventStartDate()
    {
        return eventStartDate;
    }
    void seteventStartDate(std::string val)
    {
        eventStartDate = val;
    }
    std::string geteventEndDate()
    {
        return eventEndDate;
    }
    void seteventEndDate(std::string val)
    {
        eventEndDate = val;
    }
    std::string getnumberOfSeats()
    {
        return numberOfSeats;
    }
    void setnumberOfSeats(std::string val)
    {
        numberOfSeats = val;
    }
    std::string gettypeOfEvent()
    {
        return typeOfEvent;
    }
    void settypeOfEvent(std::string val)
    {
        typeOfEvent = val;
    }
};
#endif
