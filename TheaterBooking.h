#ifndef THEATERBOOKING_H
#define THEATERBOOKING_H
#include "Event.h"
#include "Film.h"
#include <string>
class TheaterBooking
{
public:
    TheaterBooking();
    void mainMenu();
    void subMenu();
    void subSubMenu();
    void addBooking(Event e);
    void listOfAllEvents();
    void searchByEventName();
    void listOfAllbookedEvents();
    void actionOnBooking();
    bool isValidBookingId(std::string bookingId);
    std::string getBookingById(std::string bookingId);
    void updateBooking(int bookingId,std::string status);
    void updateEvent(std::string eventId,std::string seats);
    std::string getEventByid(std::string eventId);
    void addFilmEvent();
    void addStandUpComedy();
    void addLiveMusic();
protected:

private:
};

#endif // THEATERBOOKING_H
