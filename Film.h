#ifndef FILM_H
#define FILM_H

#include <string>
#include "Event.h"


class Film : public Event
{
private:
    std::string filmType;//2DOr3D
public:
    Film();
     void setfilmType( std::string val)
    {
        filmType = val;
    }
     std::string getfilmType()
    {
        return filmType;
    }

};
#endif
